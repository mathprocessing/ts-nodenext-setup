# Как написать тесты на typescript в 2023 году

# Цели, задачи
Дальше возникает вопрос: Как использовать не велосипедные фрейворки для тестирования и так чтобы
* вся автоматизация nodemon и т.д.не сломалась
* Чтобы не сломался вывод и подсветка ошибок vscode
* Чтобы можно было добавлять кастомные определения типов в `Globals.ts`
* Подключать библиотеки написанные на `js` и вручную им добавлять типы

Дополнительно:
* Такой же starter project можно создать для клиента + webpack
* И для react

# Добавление поддержки ES-modules `import something from 'something'`

0. Создаем пустую папку c файлом index.js
```js
import fs from 'fs';
console.log("Everything works fine!");
```

1. Создадим файл package.json
```
npm init -y
```

2. Добавим ключ type со значением module: `"type": "module",`
 в файл конфигурации js проекта, так мы включаем поддержку ES-модулей.
```json
{
  "name": "word-fragments-problem",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "type": "module",
  "scripts": {
    "build": "tsc"
  },
  "keywords": [],
  "author": "",
  "license": "ISC"
}
```

Пробуем запустить

```
node index.js
```
или
```
node .
```
Использовать `.` можно, потому что `index.js` запускается по умолчанию как точка входа.
Изменить точку входа можно в файле `package.json`:
`"main": "index.js",` => `"main": "my_entry_point.js",`

Ожидаемый результат запуска:
```
$ node .
Everything works fine!
```

# Магически превращаем nodejs проект в nodets проект
1. Устанавливаем последнюю версию typescript как dev-зависимость, этим мы намекаем скриптам, которые могут автоматизировать сборку (да и вообще так принято), что typescript это всего лишь инструмент, а не библиотека от которой зависит наш проект.
```
npm install typescript --save-dev
```
или
```
npm install typescript -D
```

2. Устанавливаем типы для библиотеки nodejs
```
npm i -D @types/node
```

3. Создаем файл конфигурации проекта tsconfig.json
```json
{
  "compilerOptions": {
    "module": "NodeNext",
    "moduleResolution": "NodeNext",
    "target": "ES2020",
    "sourceMap": true,
    "outDir": "dist",
  },
  "include": ["src/**/*"],
}
```

4. Добавим возможность запуска typescript компилятора `tsc` посредством `npm run build`
заменим содержимое ключа `"scripts"` в `package.json` на
```json
"scripts": {
  "build": "tsc"
},
```

5. Запускаем и проверяем, что выходной index.js софрмировался
```
npm run build
```

6. Позже можно добавить всякие плюшки

```
npm i -D ts-node
```

```json
  "scripts": {
    "build": "tsc",
    "watch": "tsc -w",
    "exec": "tsc && node dist/index.js",
    "tsnode-exec": "ts-node-esm --project tsconfig.json src/index.ts",
    "tsnode-dev": "tsnd --respawn src/index.ts"
  },
```

Можно добавить запуск при сохранении файла `index.ts`
Варианты:
* Использовать ts-node-dev 

Устанавливаем
```
npm i -D ts-node-dev
```

nodemon config `nodemon.json`
```json
{
  "execMap": {
      "ts": "ts-node-esm --project tsconfig.json src/index.ts"
  }
}
```

Пока этот вариант не настроил.
TODO: Разобраться как настроить

* Использовать nodemon (Выберем это)

Запуск из CLI:
```
npx nodemon src/index.ts --watch "./src"
```

Запуск как npm скрипта (без npx):
1. Добавить в scripts:
```json
scripts: {
  "nodemon-dev": "nodemon src/index.ts --watch ./src"
}
```
2. Теперь появилась возможность запускать используя npm:
```
npm run nodemon-dev
```

Важно:
в `tsconfig.json` мы включили флаг `"transpileOnly": true,`,
сняв с ts-node зачачу проверки типов.
Так можно делать, потому что vcode и так запускает сервер typescript'а и подсвечивает ошибки типов.
Это позволяет уменьшить время компиляции в режиме разработки.

# TODO
```ts
// Нам нужно указывать `.js` расширение, ведь импорты не преобразуются в commonjs синтаксис, а напрямую запускаются nodejs
// @todo: Как это обойти? Что мы могли писать:
// import Utils from './average';
// или хотя бы
// import Utils from './average.ts';
import Utils from './average.js';
```
# About `__dirname` in ESM(ECMAScript Modules)

Q: I have an error: "ReferenceError: __dirname is not defined in ES module scope". Can you replace "__dirname" with something else?

CGPT:
`__dirname` is a global variable that is available in Node.js, but not in the browser. You're getting a ReferenceError because you're trying to use it in an environment where it's not defined.

You can use `process.cwd()` instead of `__dirname` to get the current working directory.
```
const filePath = path.resolve(process.cwd(), '..', 'assets', 'sample.txt');
```

`process.cwd()` returns the current working directory of the Node.js process. It is equivalent to calling `process.cwd()` in Node.js.

You can also use `import.meta.url` to get the file path.

```
const filePath = new URL("../assets/sample.txt", import.meta.url).pathname;
```

This will give you the file path relative to the current javascript file.

Note: `import.meta.url` is only available in ESM(ECMAScript Modules) not in commonjs.

## В чём была проблема

Запустил
```js
fs.mkdir("dir1", (err) => {
  if (err) throw err;
});
```
и понял, что `process.cwd()` соответствует той директории, из которой запускаем `node`.

Этот код работает
```js
const data = fs.readFileSync('assets/sample.txt');
console.log(data);
```

# Links
* How to Setup Node.js with TypeScript in 2023
https://www.youtube.com/watch?v=H91aqUHn8sE

* https://stackoverflow.com/questions/62096269/cant-run-my-node-js-typescript-project-typeerror-err-unknown-file-extension

* https://blog.logrocket.com/configuring-nodemon-with-typescript/
