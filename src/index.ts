// Тестируем глобальный import, так обычно импортируются модули npm
import { open } from 'node:fs/promises';
import path from 'node:path';

// Проверяем import локальный файлов *.ts
// Нам нужно указывать `.js` расширение, ведь импорты не преобразуются в commonjs синтаксис, а напрямую запускаются nodejs
import Utils from './utilities.js';

const filePath = path.resolve(process.cwd(), 'assets', 'sample.txt');

// Тестируем подсказки IDE(vscode), работу метода
const fd = await open(filePath);
fd.createReadStream({ start: 1, end: 8 * 3 - 3 })
  .on('data', data =>
    Utils.assertEq(data.toString(), "ello1, Hello2, Hello3")
  );

// Проверяем результат запуска скрипта
const answer: number = 42;
console.log(`Everything works fine! Answer is ${answer}.`);

const input = [2, 3, 5, 7];
const result = Utils.averageArray(input);
// Это пример плохого тестирования: тут мы только печатаем результат, необходимо смотреть в консоль чтобы его чекнуть
console.log(`Среднее арифметическое [${input.join(', ')}] = ${result}`);

// Добавим проверку используя функцию `assertEq` из наших велосипедных Utils (Возиожно лучше Utils переименовать в BicyclesAndCrutches)
Utils.assertEq(result, 4.25); // Теперь 4.24 уже не прокатит