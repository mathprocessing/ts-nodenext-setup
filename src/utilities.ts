namespace Utils {
  /**
   * Замена `console.assert` для написания тестов "на коленке"
   */
  export function assert(value: boolean, msg: string) {
    if (!value) { // Можно написать `value !== true`, но проверки типов должно быть достаточно
      throw new Error(`Assertion Error: ${msg}`);
    }
  }

  // TODO: Как записать ограничение на тип <T entends Eq> в Typescript? `a` и `b` должны быть сравнимы с помощью оператора `===` и этот факт должен устанавливаться в процессе компиляции (Compile time: ts -> js), а не во время запуска (js)
  /**
   * Гарантирует, что первый аргумент строго `===` равен второму, иначе возращает ошибку с парой различных значений.
   * 
   * TODO:
   * Сообщение об ошибке выглядит так:
   * ```text
   *                 throw new Error(`Assertion Error: '${a}' !== '${b}'`);
   *                       ^
   *
   * Error: Assertion Error: 'ello1, H' !== 'ello1, H5'
   * ```
   * 
   * Можно улучшить заменив как-то `Error: Assertion Error:` на `Assertion Error:`
   * Иногда возращает кашу: ' !== 'ello1, Hello2, Hello3'1, Hello2, Hello3 поэтому для тестирования лучше использовать jest например
   * 
   * P.S. Лучше наверное вынести этот todo из документации в issues.
   */
  export function assertEq<T>(a: T, b: T) {
    if (a !== b) {
      // Стороки некрасиво отображаются, поэтому для печатаем сообщение отдельно
      if (typeof a === 'string') {
        throw new Error(`Assertion Error: '${a}' !== '${b}'`);
      } else {
        throw new Error(`Assertion Error: ${a} !== ${b}`);
      }
    }
  }

  /**
   * Вычисляет среднее арифметическое для элементов массива `a`
   * @param a Входной массив чисел
   */
  export function averageArray(a: number[]): number {
    let acc = 0;
    for (let i = 0; i < a.length; i++) {
      acc += a[i];
    }
    return acc / a.length;
  }

  /**
   * Вычисляет сумму элементов массива используя функцию `reduce`
   * @param a Входной массив чисел
   */
  export function sumArray(a: number[]): number {
    return a.reduce((x, y) => x + y, 0);
  }
}

export default Utils;